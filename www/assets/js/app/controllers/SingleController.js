angular
	.module('BomNegocio')
	.controller('SingleController', SingleController);

SingleController.$inject = [ 
'$interval',
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'HouseService',
];

function SingleController($interval, $window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, HouseService){
	$scope.displayImage = null;
	var f = $interval(function(){
		$scope.house = HouseService.getHouse();	
		var $ad = $scope.house.adv.imob_adicionais;
		if( typeof $ad == 'string'  ){
			var currentAd = $ad.split(',');			
			currentAd = currentAd.map(function(x){
				return x.match(/[A-Z][a-z]+/g);
			})
			console.log(currentAd);
			$scope.house.adv.imob_adicionais = currentAd;
		}

	},2000);

	$scope.back = function(route){
		$state.go(route);
	};
	$scope.expand = function() {
		if( !$scope.expandImage ){
			$scope.expandImage = true;
			return;
		}

		$scope.expandImage = null;
	};	

	$scope.alter = function(img){
		$scope.house.adv.image = img;
	};

	console.log($scope.house)
			$scope.house = HouseService.getHouse();			

	$scope.toggleContact = function(){
		if( $scope.showContact ){
			$scope.showContact = false
			return;
		}

		$scope.showContact = true;
	}
	if( !$scope.house){
		$state.go('/houses');
		HouseService.setHouse(null);
		return;
	}

	$scope.someFunction = function(){
		console.log('to right?');
	}

	$timeout(function(){
		$scope.$apply(function(){
			$scope.house = HouseService.getHouse();			
			console.log('YTOING TO GET.... x->',$scope.house)
			var t=[];

			for( var i =0; i< 20; i++ ){
				t.push($scope.house.adv['gal_image'+i])				
			}		


			$scope.house.gallery = t.filter(function(val){ return val != undefined && val != null });
			if( $scope.house.adv.image )
				$scope.house.gallery.push($scope.house.adv.image);
			
			$scope.house.gallery.reverse();
			console.log('YTOING TO GET.... x->',$scope.house)

		});
	}, 1000)
};