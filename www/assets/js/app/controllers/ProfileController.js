angular
	.module('BomNegocio')
	.controller('ProfileController', ProfileController);

ProfileController.$inject = [ 
    '$window',
    '$interval',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'User',
    'HouseService',
    'Settings'
];

function ProfileController($window, $interval, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, User, HouseService,Settings){
    
    $scope.user = User.getUser();
    $images = $window.localStorage.getItem('user_avatar');

    var _img = $images;

    _img = JSON.parse($images);

    if( _img ){

        console.log('onload', _img)

    }



    $scope.base = Settings.API_BASE;
    $scope.stateLoading = true;
    if(  !$scope.user) $state.go('/login')
    $scope.selectFile = function(target){
        setTimeout(function () {
          angular.element(target).trigger('click');
        }, 0);

    };
    $scope.view = function(house){

        HouseService.setHouse(house);
        $state.go('house', {house: house});
    };    
    $scope.imageUpload = function(event, target){
         var files = event.target.files;

         for (var i = 0; i < files.length; i++) {
             var file = files[i];

                var reader = new FileReader();

                reader.onload = function(e){
                    $scope.user = User.getUser();
                    $images = $window.localStorage.getItem('user_avatar');

                    if( !$images ){
                        $window.localStorage.setItem('user_avatar', '[]');
                        $images = $window.localStorage.getItem('user_avatar');

                    }
                    delete $scope.user.avatar;
                    var _img = $images;

                    _img = JSON.parse($images);
             
                   

                    var y = _img.filter(function(x){ 
                        if(x.id == $scope.user.id ){
                            x.value = e.target.result
                        }
                        delete x;
                    });
                    y.push({id: $scope.user.id, value: e.target.result});
                    console.log(_img, y)
                    $window.localStorage.setItem('user_avatar', JSON.stringify(y));



                    $scope.user.avatar = e.target.result;
                    User.setUser(JSON.stringify($scope.user));
                    angular.element('.uavatar')[0].src=$scope.user.avatar;
                };

                reader.readAsDataURL(file);
         }
    };
    console.log('user', $scope.user, HouseService.getHouses());
    var f = $interval(function(){

        $scope.user = User.getUser();
        if( !$scope.user ) return;
        $images = $window.localStorage.getItem('user_avatar');
        var _img = JSON.parse($images);
        if( _img ){
        console.log('onloop', _img)

            _img = _img.filter(function(x){ return x; })
            _img = _img.filter(function(x){ return x.id == $scope.user.id })
            if( _img[0] )
                $scope.user.avatar = _img[0].value;
        }


        // console.log('state', )

        // console.log(!$scope.user &&$state.url != '/houses',$scope.user,$state.current, '/houses')
        if(!$scope.user &&$state.current.url == '/profile'){
            $state.go('/login');
        }

        // if( !$scope.user ){
        // }
    
        if( !HouseService.getHouses() ){
            // console.log('no houses...')
            HouseService.fetch(function(data){      
                $rootScope.houses = HouseService.getHouses();
                $scope.count = HouseService.getCount();
                $scope.stateLoading = false;
                 if( HouseService.getHouses() ){
                    if( !$rootScope.myHouses )
                $rootScope.myHouses = HouseService.getHouses().filter(function(e){
                    return e.adv.user_id ==  $scope.user.id;
                })
            }
            })      
            return;     
        }

        if( HouseService.getHouses() ){
            if( !$rootScope.myHouses )
            $rootScope.myHouses = HouseService.getHouses().filter(function(e){
                // console.log('hmmmmmmm',e.adv.user_id, $scope.user.id,e.adv)
                return e.adv.user_id ==  $scope.user.id;
            })                
        }else{
            
        }    
    },1000)

    if( !HouseService.getHouses() ){
	   	HouseService.fetch(function(data){      
    	    $rootScope.houses = HouseService.getHouses();
            $scope.count = HouseService.getCount();
    	    $scope.stateLoading = false;
            if( !$rootScope.myHouses )
			$rootScope.myHouses = HouseService.getHouses().filter(function(e){
				return e.adv.user_id ==  $scope.user.id;
			})
	   	})    	
		return;	   	
    }
    if( $scope.user ){
        if( !$rootScope.myHouses )
    	$rootScope.myHouses = HouseService.getHouses().filter(function(e){
    		return e.adv.user_id ==  $scope.user.id;
    	})
    }
};