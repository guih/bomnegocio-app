angular
	.module('BomNegocio')
	.controller('HousesController', HousesController);

HousesController.$inject = [ 
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'HouseService',
    '$interval',
    'User'
];

function HousesController($window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, HouseService, $interval, User){
    $scope.stateLoading = true;
    HouseService.fetch(function(data){ 

$scope.mascaraValor = function(valor) {
    valor = valor.toString().replace(/\D/g,"");
    valor = valor.toString().replace(/(\d)(\d{8})$/,"$1.$2");
    valor = valor.toString().replace(/(\d)(\d{5})$/,"$1.$2");
    valor = valor.toString().replace(/(\d)(\d{2})$/,"$1,$2");
    return valor                    
}
    
	    $rootScope.houses = HouseService.getHouses();
        if( $rootScope.houses ){          

            $rootScope.houses = $rootScope.houses.map(function(e){
                e.title = e.adv.title.match(/\([^)]*\)|\[[^\]]*\]/);
                if( e.title ){
                    e.title = e.title[0]
                }          
                e.money = $scope.mascaraValor(e.adv.team_price);;
                return e;
            })
             $rootScope.houses = $rootScope.houses.map(function(e){
                if( !e.adv.team_adicionais ) return e;
                    e.adicionais = (e.adv.team_adicionais).split(',');
                    e.adicionais = e.adicionais.map(function(x){
                        return x.match(/[A-Z][a-z]+/g).join( ' ' ); 
                    })
                    return e;
                })        
            $scope.count = HouseService.getCount();
    	    $scope.stateLoading = false;
        }
  
   	})

    $rootScope.houses = HouseService.getHouses();
    $interval(function(){

        $rootScope.houses = HouseService.getHouses();
   $rootScope.houses = $rootScope.houses.map(function(e){
                e.title = e.adv.title.match(/\([^)]*\)|\[[^\]]*\]/);
                if( e.title ){
                    e.title = e.title[0]
                }          
                e.money = $scope.mascaraValor(e.adv.team_price);
                return e;
            })        
            if( !$rootScope.userHouses ){
                $scope.user = User.getUser()
           User.getImo($scope.user.id, function(houses){
                $rootScope.userHouses = houses.data;
            });      
            }
    },1000)
    $scope.count = HouseService.getCount();

    $scope.search = function(){
        console.log($scope.search_query, '<<<<<<<<<<')
        if( $scope.search_query == "" || $scope.search_query == null || $scope.search_query == undefined ){

            return false;
        }
        $rootScope.houses = null;
        $scope.stateLoading = true;


        HouseService.searchHouses($scope.search_query, function(houses){
             if( houses ){     
                $rootScope.houses= houses;
           $rootScope.houses = $rootScope.houses.map(function(e){
                e.title = e.adv.title.match(/\([^)]*\)|\[[^\]]*\]/);
                if( e.title ){
                    e.title = e.title[0]
                }
                e.money = $scope.mascaraValor(e.adv.team_price);
                return e;
            })     
            $rootScope.houses = $rootScope.houses.map(function(e){
            if( !e.adv.team_adicionais ) return e;
                
                e.adicionais = (e.adv.team_adicionais).split(',');
                e.adicionais = e.adicionais.map(function(x){
                    return x.match(/[A-Z][a-z]+/g).join( ' ' ); 
                })
                return e;
            })     
            $rootScope.houses = HouseService.getHouses();
            $scope.count = HouseService.getCount();

            $scope.stateLoading = false;
        }
        });
    }
    $scope.toggleFilters = function(){
    	if( $scope.showFilters ){

    		$scope.showFilters =false;
    		return;
    	}

    	$scope.showFilters = true;
    }
    $scope.sortRecent = function(){
        $rootScope.houses = $rootScope.houses.sort(function(a,b){
            var a = new Date(parseInt(a.adv.begin_time) * 1000 );
            var b = new Date(parseInt(b.adv.begin_time) * 1000 );
            return  b.getTime() - a.getTime();
        });    
        $scope.toggleFilters();            
    }

    $scope.sortHigh = function(){
        $rootScope.houses = $rootScope.houses.sort(function(a,b){  
            return parseInt( b.adv.team_price ) > parseInt(a.adv.team_price);
        }); 
        $scope.toggleFilters();               
    }
    $scope.sortLower = function(){
        $rootScope.houses = $rootScope.houses.sort(function(a,b){  
            return parseInt( b.adv.team_price ) < parseInt(a.adv.team_price);
        });  
        $scope.toggleFilters();              
    }
    $scope.view = function(house){

    	HouseService.setHouse(house);
    	$state.go('house', {house: house});
    };
};