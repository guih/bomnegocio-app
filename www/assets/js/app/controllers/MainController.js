angular
	.module('BomNegocio')
	.controller('MainController', MainController);

MainController.$inject = [ 
'$interval',
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'User'
];

function MainController($interval, $window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, User){
    $scope.user = {};

    $timeout(function(){
        var isUser = $window.localStorage.getItem('user');
        var user = JSON.parse(isUser);

        if( user ){
            User.setUser(JSON.stringify(user));            
            $state.go('/houses');
        }else{
        }
        if( !$rootScope.userHouses ){
                $scope.user = User.getUser()
           User.getImo($scope.user.id, function(houses){
                $rootScope.userHouses = houses.data;
            });      
            }
 
    },1000);

    $scope.login = function(){
        User.login($scope.user).then(function(res){
            var data = res.data;

            if( data.error ){
                $scope.message = data.error;
            }else{
                User.setUser(JSON.stringify(data));
                
                $state.go('/houses', JSON.stringify(data), { reload: true });
            }
        });
    }

    $scope.logout = function(){
        User.erase(function(){
            $state.go('/houses');
        });
    }
};