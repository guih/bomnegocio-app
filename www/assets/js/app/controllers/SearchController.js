angular
	.module('BomNegocio')
	.controller('SearchController', SearchController);

SearchController.$inject = [ 
'$interval',
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'HouseService'
];

function SearchController($interval, $window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, HouseService){
    $scope.search = {
    };
    // $scope.isLoadingSearch = !$scope.isLoadingSearch;
    $interval(function(){
        $scope.isLoadingSearch = false;
    },5000)
    $scope.types = [{id: 4, label: 'Rural'},
{id: 6 , label: 'Apartamento'},
{id: 7 , label: 'Apart Hotel'},
{id: 8 , label: 'Área Privativa'},
{id: 9 , label: 'Barracão'},
{id: 11, label: 'Casa'},
{id: 12, label: 'Casa em condomínio'},
{id: 13, label: 'Chácara'},
{id: 14, label: 'Clube'},
{id: 15, label: 'Cobertura'},
{id: 19, label: 'Estacionamento/Garagem'},
{id: 21, label: 'Fazenda'},
{id: 22, label: 'Flat'},
{id: 23, label: 'Galpão'},
{id: 25, label: 'Haras'},
{id: 26, label: 'Hotel'},
{id: 29, label: 'Quitinete'},
{id: 30, label: 'Loft'},
{id: 31, label: 'Loja'},
{id: 33, label: 'Lote'},
{id: 37, label: 'Prédio'},
{id: 38, label: 'Sala'},
{id: 40, label: 'Sítio'},
{id: 41, label: 'Sobrado'},];

    $scope.searchByFilters = function(){
        console.log($scope.search)
        $scope.isLoadingSearch = true;
        if( $scope.search.cidade)
            $scope.search.cidade =  JSON.parse($scope.search.cidade);
        HouseService.filterHouses($scope.search, function(houses){
            $state.go('/houses')
        });
        return;
        HouseService.searchHouses($scope.search_query, function(houses){
            
            $rootScope.houses = HouseService.getHouses();
            $scope.count = HouseService.getCount();
            $scope.isLoadingSearch = !$scope.isLoadingSearch;
            
            $state.go('/houses');


        });
    }
    $scope.setCity = function(city){
        console.log('ST')
        $scope.cityX = city;
    };

    $scope.updateHood = function(){
        $scope.isLoadingSearch = true;

        var cidade = JSON.parse($scope.search.cidade);
        HouseService.fetchHood(cidade.id, function(hoods){


        console.log(hoods, '<<<<<<<<')
            if( !hoods || hoods == null || hoods == 'null'){
                hoods = [];
            }


            $scope.hoods = hoods;
        $scope.isLoadingSearch = false;

        });
    };
    $scope.updateCitys = function(){
        $scope.isLoadingSearch = true;
        HouseService.fetchCitys($scope.search.estado, function(citys){
            $scope.citys = citys;
        $scope.isLoadingSearch = false;

        });
    };
    $scope.clearFilters = function () {
        $scope.my_object_backup = {};
        angular.copy($scope.search, $scope.my_object_backup);
    }
};