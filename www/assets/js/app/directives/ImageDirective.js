angular
  .module('BomNegocio').directive('imgFadeInOnload', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attr) {
        element.css('opacity', 0);
        element.css('-moz-transition', 'opacity 2s');
        element.css('-webkit-transition', 'opacity 2s');
        element.css('-o-transition', 'opacity 2s');
        element.css('transition', 'opacity 2s');
        element.bind("load", function () {
          element.css('opacity', 1);
        });
        element.attr('src', attr.imgFadeInOnload);
      }
    };
  });

    //   if( isset($_GET['filters']) ){
    //     if( isset($_GET['finalidade']) ){
    //         $query .= "and imob_finalidade = '".$_GET['finalidade']."'";
    //     }
    //     if( isset($_GET['tipo']) ){
    //         $query .= "and team_type = '".$_GET['tipo']."'";
    //     }     
    //     if( isset($_GET['quartos']) ){
    //         $query .= "and imob_quartos = '".$_GET['quartos']."'";
    //     }  
    //     if( isset($_GET['vagas']) ){
    //         $query .= "and imob_quartos = '".$_GET['vagas']."'";
    //     }          
    //   if( isset($_GET['cidade']) ){
    //         $query .= "and imob_cidade = '".$_GET['cidade']."'";
    //     }  
    //   if( isset($_GET['estado']) ){
    //         $query .= "and imob_estado = '".$_GET['estado']."'";
    //     }        
    //     if( isset($_GET['condominio']) ){
    //         $query .= "and imob_condominio = '".$_GET['conodminio']."'";
    //     }  
    //     if( isset($_GET['bairro']) ){
    //         $query .= "and imob_bairro = '".$_GET['bairro']."'";
    //     }  
    // }