angular
    .module('BomNegocio')
        .factory('HouseService', HouseService);
HouseService.$inject = [
    '$q', 
    '$http',
    'Settings'
];

function HouseService($q, $http, Settings) {
	var HouseService = {};

	
	HouseService.fetch = function(fn){
		if( HouseService.houses != null ){
			return fn(HouseService.houses);
		}

		$http.get(Settings.API_BASE+'autenticacao/imoveis.php', {}).then(function(houses){
			HouseService.setCount( houses.data.meta.count );
			HouseService.setHouses( houses.data );
			return fn( HouseService.houses )
		});
	};

	HouseService.setHouse = function(house){
		HouseService.house = house;
	};
	HouseService.getHouse = function(){
		return HouseService.house || {};
	};
	HouseService.fetchHood = function(city, fn){
		$http.get(Settings.API_BASE+'autenticacao/imoveis.php?fetch=&hood=&city='+city, {})
		.then(function(hoods){	
		if( hoods.data == 'null' ) hoods.data = [];

			return fn( hoods.data )
		});
	}
	HouseService.fetchCitys = function(state, fn){
		$http.get(Settings.API_BASE+'autenticacao/imoveis.php?fetch=&state='+state, {})
		.then(function(citys){

		
			return fn( citys.data )
		});
	}

	HouseService.searchHouses = function(q, fn){
		$http.get(Settings.API_BASE+'autenticacao/imoveis.php?find='+q, {}).then(function(houses){
			HouseService.setCount( houses.data.meta.count );
			
			HouseService.setHouses( houses.data );
			return fn( HouseService.houses )
		});
	}

	HouseService.filterHouses = function(q, fn){
		if( q.cidade )
			q.cidade = q.cidade.nome;
		$http.get(Settings.API_BASE+'autenticacao/imoveis.php?filters=&'+jQuery.param( q ), {}).then(function(houses){
			HouseService.setCount( houses.data.meta.count );
			
			HouseService.setHouses( houses.data );
			return fn( HouseService.houses )
		});
	}
	HouseService.getHouses = function(){
		return HouseService.houses || null;
	};
	HouseService.getCount = function(){
		return HouseService.count
	};
	HouseService.setCount = function(count){
		HouseService.count = count;
	};
	HouseService.setHouses = function(houses){		
		console.log('HOUSE >', houses);
		HouseService.houses = houses.data;	
	}	

	return HouseService;
};