angular
    .module('BomNegocio')
        .factory('User', User);
User.$inject = [
    '$q', 
    '$http',
    '$window',
    'Settings',
    '$state'
];

function User($q, $http, $window, Settings) {
	var User = {};

	User.login = function(data){
		data.acao = 'REST_LOGIN';

		return $http.post(Settings.API_BASE+'autenticacao/login.php', data);
	};
	User.setUser = function(user){
		$window.localStorage.setItem('user', user);
	};
	User.getUser = function(){
		console.log('get user?')
		var user = JSON.parse($window.localStorage.getItem('user'));
		// if( ! )
		return user;
	};

	User.getImo = function(id, fn){
		$http.get(Settings.API_BASE+'autenticacao/imoveis.php?fetch=&housing=&uid='+id, {}).then(function(houses){
			return fn( houses.data )
		});		
	}
	User.getHouses = function(){
		return User.houses || {};
	};
	User.erase = function(fn){
		$window.localStorage.removeItem('user');
		fn();
	}
	User.setHouses = function(houses){		
		console.log('HOUSE >', houses);
		User.houses = houses.data;	
	}	

	return User;
};