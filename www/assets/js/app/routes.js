angular
	.module('BomNegocio', [
		'ionic',
		'ngMask',
		'ui.router',
		'ngAnimate'
	]).config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider',
	function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

		$ionicConfigProvider.views.transition('none');

		$stateProvider.
			state('/login', {
				url: '/login',
				templateUrl: 'templates/splash.html',
				controller: 'MainController',
			}).
			state('house', {
				url: '/house',
				templateUrl: 'templates/single.html',
				controller: 'SingleController',
			}).			
			state('/houses', {
				url: '/houses',
				templateUrl: 'templates/houses.html',
				controller: 'HousesController',
			}).
			state('/search', {
				url: '/search',
				templateUrl: 'templates/search.html',
				controller: 'SearchController',
			}).
			state('/profile', {
				url: '/profile',
				templateUrl: 'templates/profile.html',
				controller: 'ProfileController',
			})
			

		$urlRouterProvider.otherwise('/houses');

	}]).run(function($timeout,$window,$ionicPlatform, $q, $state, $rootScope) {

		console.log('app ready')
		    
	});